import 'dart:developer';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class ApiClient {
  static final Map<String, String> _defaultHeaders = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
    'Authorization': '${dotenv.env['API_KEY']}',
  };

  static Future get(String url, {bool? isAuth, Map? params}) async {
    final uri = Uri.parse(_queryParamGenerator(url, params));
    if (isAuth ?? false) {
      final res = await http.get(uri, headers: _defaultHeaders);
      return res.body;
    } else {
      final res = await http.get(uri);
      return res.body;
    }
  }

  static Future post(String url, {bool? isAuth, Map? body, Map? params}) async {
    final uri = Uri.parse(_queryParamGenerator(url, params));
    if (isAuth ?? false) {
      final res = await http.post(uri, headers: _defaultHeaders, body: body);
      return res.body;
    } else {
      final res = await http.post(uri, body: body);
      return res.body;
    }
  }

  // This function will generate url with query parameter if given
  // will return the url if param == null
  static String _queryParamGenerator(String url, Map? params) {
    if (params != null) {
      String tmp = '?';
      for (var obj in params.entries) {
        tmp += '${obj.key}=${obj.value}&';
      }

      log('url ${url + tmp.substring(0, tmp.length - 1)} header $_defaultHeaders');
      // remove ampersand in the end of generated query
      return url + tmp.substring(0, tmp.length - 1);
    }
    log('url $url header $_defaultHeaders');
    return url;
  }
}
