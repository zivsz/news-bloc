import 'package:go_router/go_router.dart';
import 'package:news_bloc/constants/app_path.dart';
import 'package:news_bloc/modules/homepage/presentation/screen/homepage.dart';
import 'package:news_bloc/modules/homepage/presentation/screen/news_detail_screen.dart';

final GoRouter globalRouter = GoRouter(
  routes: [
    GoRoute(
      path: AppPath.main,
      builder: (context, state) => const Homepage(),
    ),
    GoRoute(
      path: AppPath.newsDetail,
      builder: (context, state) => const NewsDetailScreen(),
    ),
  ],
);
