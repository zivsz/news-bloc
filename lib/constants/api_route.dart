class ApiRoute {
  static const _baseUrl = 'https://newsapi.org/v2';

  static String everything = '$_baseUrl/everything';
  static String headline = '$_baseUrl/top-headlines';
}
