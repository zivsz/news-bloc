import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'news.freezed.dart';
part 'news.g.dart';

@freezed
class News with _$News {
  factory News({
    @Default('') String? status,
    @Default(0) int? totalResults,
    required List<Article> articles,
  }) = _News;

  factory News.fromJson(Map<String, Object?> json) => _$NewsFromJson(json);
}

@freezed
class Article with _$Article {
  factory Article({
    required Source source,
    @Default('') String? author,
    @Default('') String? title,
    @Default('') String? description,
    @Default('') String? url,
    @Default('') String? urlToImage,
    @Default('') String? publishedAt,
    @Default('') String? content,
  }) = _Article;

  factory Article.fromJson(Map<String, Object?> json) => _$ArticleFromJson(json);
}

@freezed
class Source with _$Source {
  factory Source({
    @Default('') String? id,
    @Default('') String? name,
  }) = _Source;

  factory Source.fromJson(Map<String, Object?> json) => _$SourceFromJson(json);
}
