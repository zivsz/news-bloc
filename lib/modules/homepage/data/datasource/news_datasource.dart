import 'dart:convert';

import 'package:news_bloc/core/api_client.dart';
import 'package:news_bloc/constants/api_route.dart';

class NewsDatasource {
  Future getEverything({Map? params}) async {
    return await ApiClient.get(ApiRoute.everything, isAuth: true, params: params);
  }

  Future getHeadlines({Map? params}) async {
    return await ApiClient.get(ApiRoute.headline, isAuth: true, params: params);
  }
}
