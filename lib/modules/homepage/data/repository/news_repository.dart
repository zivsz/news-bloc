import 'dart:convert';

import 'package:news_bloc/modules/homepage/data/datasource/news_datasource.dart';
import 'package:news_bloc/modules/homepage/models/news.dart';

class NewsRepository {
  final NewsDatasource newsDatasource;

  NewsRepository({required this.newsDatasource});

  Future<News> getEverything({Map? params}) async {
    try {
      final res = await newsDatasource.getEverything(params: params);
      return News.fromJson(jsonDecode(res));
    } catch (e) {
      throw e.toString();
    }
  }

  Future<News> getHeadline({Map? params}) async {
    try {
      final res = await newsDatasource.getHeadlines(params: params);
      return News.fromJson(jsonDecode(res));
    } catch (e) {
      throw e.toString();
    }
  }
}
