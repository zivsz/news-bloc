import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_bloc/modules/homepage/bloc/news/news_bloc.dart';

class SearchbarWidget extends StatelessWidget {
  const SearchbarWidget({
    super.key,
    required this.searchController,
  });

  final TextEditingController searchController;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      margin: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextField(
        controller: searchController,
        onSubmitted: (value) {
          if (searchController.text.isNotEmpty) {
            context.read<NewsBloc>().add(SearchNews(value: searchController.text));
          } else {
            context.read<NewsBloc>().add(SearchNews(value: 'technology'));
          }
        },
        decoration: const InputDecoration(
          hintText: 'Search',
          border: InputBorder.none,
        ),
      ),
    );
  }
}
