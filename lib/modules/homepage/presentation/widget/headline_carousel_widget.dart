import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:news_bloc/constants/app_path.dart';
import 'package:news_bloc/modules/homepage/bloc/news_detail/news_detail_bloc.dart';
import 'package:news_bloc/modules/homepage/models/news.dart';

class HeadlineCarouselWidget extends StatelessWidget {
  const HeadlineCarouselWidget({
    super.key,
    required this.data,
  });

  final News data;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      child: ListView.separated(
        separatorBuilder: (context, index) => const SizedBox(width: 10),
        padding: const EdgeInsets.symmetric(horizontal: 20),
        scrollDirection: Axis.horizontal,
        itemCount: data.articles.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              context.push(AppPath.newsDetail);
              context.read<NewsDetailBloc>().add(NewsDetail(article: data.articles[index]));
            },
            child: Stack(
              children: [
                Container(
                  height: 200,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  foregroundDecoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: const LinearGradient(
                      colors: [Colors.transparent, Colors.transparent, Colors.black],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: [0, 0.5, 1],
                    ),
                  ),
                  child: data.articles[index].urlToImage!.isNotEmpty
                      ? SizedBox(
                          width: 200,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: CachedNetworkImage(
                              imageUrl: data.articles[index].urlToImage!,
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                      : const SizedBox(
                          width: 200,
                          child: Center(
                            child: Text('No Image'),
                          ),
                        ),
                ),
                Positioned.fill(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 5),
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        data.articles[index].title ?? '',
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
