import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:news_bloc/constants/app_path.dart';
import 'package:news_bloc/modules/homepage/bloc/news_detail/news_detail_bloc.dart';
import 'package:news_bloc/modules/homepage/models/news.dart';

class NewsListWidget extends StatelessWidget {
  const NewsListWidget({
    super.key,
    required this.data,
  });

  final News data;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.only(bottom: 10, left: 20, right: 20),
      separatorBuilder: (context, index) => const SizedBox(height: 15),
      itemCount: data.articles.length,
      itemBuilder: (context, index) => GestureDetector(
        onTap: () {
          context.push(AppPath.newsDetail);
          context.read<NewsDetailBloc>().add(NewsDetail(article: data.articles[index]));
        },
        child: Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                data.articles[index].title ?? 'null',
                style: const TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(data.articles[index].description ?? 'null')
            ],
          ),
        ),
      ),
    );
  }
}
