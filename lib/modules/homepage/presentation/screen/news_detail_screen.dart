import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:news_bloc/modules/homepage/bloc/news_detail/news_detail_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsDetailScreen extends StatelessWidget {
  const NewsDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      appBar: AppBar(
        title: const Text('News Detail'),
      ),
      body: BlocBuilder<NewsDetailBloc, NewsDetailState>(
        builder: (context, state) {
          if (state is NewsSeeDetail) {
            return Container(
              margin: const EdgeInsets.all(20),
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    state.article.title ?? '',
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      const Text('Author : ', style: TextStyle(fontSize: 12)),
                      Text(
                        state.article.author ?? 'unknown',
                        style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      const Text('Published at : ', style: TextStyle(fontSize: 12)),
                      Text(
                        DateFormat('dd-MMM-yyyy').format(
                          DateTime.parse(
                            state.article.publishedAt ?? DateTime.now().toString(),
                          ),
                        ),
                        style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  CachedNetworkImage(imageUrl: state.article.urlToImage ?? ''),
                  const SizedBox(height: 10),
                  Text(state.article.content ?? ''),
                  const SizedBox(height: 10),
                  RichText(
                    text: TextSpan(
                      children: [
                        const TextSpan(
                          text: 'Read more at ',
                          style: TextStyle(color: Colors.black),
                        ),
                        TextSpan(
                          text: state.article.url ?? '',
                          style: const TextStyle(color: Colors.blue),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              if (state.article.url != null) {
                                if (!await launchUrl(
                                  Uri.parse(state.article.url!),
                                  mode: LaunchMode.externalApplication,
                                )) {
                                  throw Exception('Could not launch url');
                                }
                              }
                            },
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            );
          } else {
            return const SizedBox();
          }
        },
      ),
    );
  }
}
