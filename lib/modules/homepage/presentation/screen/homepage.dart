import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_bloc/modules/homepage/bloc/news/news_bloc.dart';
import 'package:news_bloc/modules/homepage/bloc/news_headline/news_headline_bloc.dart';
import 'package:news_bloc/modules/homepage/models/news.dart';
import 'package:news_bloc/modules/homepage/presentation/widget/headline_carousel_widget.dart';
import 'package:news_bloc/modules/homepage/presentation/widget/news_list_widget.dart';
import 'package:news_bloc/modules/homepage/presentation/widget/searchbar_widget.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    context.read<NewsBloc>().add(NewsFetched());
    context.read<NewsHeadlineBloc>().add(NewsHeadlineFetched());
    super.initState();
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                'Top Headlines',
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(height: 5),
            BlocBuilder<NewsHeadlineBloc, NewsHeadlineState>(
              builder: (context, state) {
                if (state is NewsHeadlineError) {
                  return Text(state.error);
                }
                if (state is! NewsHeadlineSuccess) {
                  return const Center(
                    child: CircularProgressIndicator.adaptive(),
                  );
                }
                final data = state.news;
                return HeadlineCarouselWidget(data: data);
              },
            ),
            const SizedBox(height: 10),
            SearchbarWidget(searchController: searchController),
            const SizedBox(height: 10),
            Expanded(
              child: BlocBuilder<NewsBloc, NewsState>(
                builder: (context, state) {
                  if (state is NewsFailed) {
                    return Center(
                      child: Text(state.error),
                    );
                  }
                  if (state is! NewsSuccess) {
                    return const Center(
                      child: CircularProgressIndicator.adaptive(),
                    );
                  }
                  final data = state.news;
                  return NewsListWidget(data: data);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
