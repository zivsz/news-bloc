part of 'news_headline_bloc.dart';

abstract class NewsHeadlineState {}

class NewsHeadlineInitial extends NewsHeadlineState {}

class NewsHeadlineLoading extends NewsHeadlineState {}

class NewsHeadlineError extends NewsHeadlineState {
  final String error;

  NewsHeadlineError({required this.error});
}

class NewsHeadlineSuccess extends NewsHeadlineState {
  final News news;

  NewsHeadlineSuccess({required this.news});
}
