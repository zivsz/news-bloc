import 'package:bloc/bloc.dart';
import 'package:news_bloc/modules/homepage/data/repository/news_repository.dart';
import 'package:news_bloc/modules/homepage/models/news.dart';

part 'news_headline_event.dart';
part 'news_headline_state.dart';

class NewsHeadlineBloc extends Bloc<NewsHeadlineEvent, NewsHeadlineState> {
  final NewsRepository newsRepository;

  NewsHeadlineBloc(this.newsRepository) : super(NewsHeadlineInitial()) {
    on<NewsHeadlineFetched>(_newsHeadlineFetch);
  }

  void _newsHeadlineFetch(NewsHeadlineFetched event, Emitter<NewsHeadlineState> emit) async {
    emit(NewsHeadlineLoading());

    try {
      Map params = {
        'country': 'us',
        'pageSize': 10,
        'page': 1,
      };
      final news = await newsRepository.getHeadline(params: params);
      emit(NewsHeadlineSuccess(news: news));
    } catch (e) {
      emit(NewsHeadlineError(error: e.toString()));
    }
  }
}
