part of 'news_detail_bloc.dart';

abstract class NewsDetailEvent {}

final class NewsDetail extends NewsDetailEvent {
  final Article article;
  NewsDetail({required this.article});
}
