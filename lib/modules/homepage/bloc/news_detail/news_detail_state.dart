part of 'news_detail_bloc.dart';

abstract class NewsDetailState {}

class NewsDetailInitial extends NewsDetailState {}

class NewsSeeDetail extends NewsDetailState {
  final Article article;

  NewsSeeDetail({required this.article});
}
