import 'package:bloc/bloc.dart';
import 'package:news_bloc/modules/homepage/models/news.dart';

part 'news_detail_event.dart';
part 'news_detail_state.dart';

class NewsDetailBloc extends Bloc<NewsDetailEvent, NewsDetailState> {
  NewsDetailBloc() : super(NewsDetailInitial()) {
    on<NewsDetail>(_newsDetail);
  }

  void _newsDetail(NewsDetail event, Emitter<NewsDetailState> emit) {
    emit(NewsSeeDetail(article: event.article));
  }
}
