part of 'news_bloc.dart';

abstract class NewsState {}

class NewsInitial extends NewsState {}

class NewsLoading extends NewsState {}

class NewsSuccess extends NewsState {
  final News news;

  NewsSuccess({required this.news});
}

class NewsFailed extends NewsState {
  final String error;

  NewsFailed({required this.error});
}
