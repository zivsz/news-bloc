part of 'news_bloc.dart';

abstract class NewsEvent {}

final class NewsFetched extends NewsEvent {}

final class SearchNews extends NewsEvent {
  final String value;
  SearchNews({required this.value});
}
