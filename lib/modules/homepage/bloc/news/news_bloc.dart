import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_bloc/modules/homepage/data/repository/news_repository.dart';
import 'package:news_bloc/modules/homepage/models/news.dart';

part 'news_event.dart';
part 'news_state.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  final NewsRepository newsRepository;

  NewsBloc(this.newsRepository) : super(NewsInitial()) {
    on<NewsFetched>(_getNewsEverything);
    on<SearchNews>(_searchNews);
  }

  void _getNewsEverything(NewsFetched event, Emitter<NewsState> emit) async {
    emit(NewsLoading());

    try {
      Map params = {
        'q': 'technology',
        'pageSize': 10,
        'page': 1,
      };
      final news = await newsRepository.getEverything(params: params);
      emit(NewsSuccess(news: news));
    } catch (e) {
      emit(NewsFailed(error: e.toString()));
    }
  }

  void _searchNews(SearchNews event, Emitter<NewsState> emit) async {
    emit(NewsLoading());

    try {
      Map params = {
        'q': event.value,
        'pageSize': 10,
        'page': 1,
      };
      final news = await newsRepository.getEverything(params: params);
      emit(NewsSuccess(news: news));
    } catch (e) {
      emit(NewsFailed(error: e.toString()));
    }
  }
}
