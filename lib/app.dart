import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_bloc/core/router.dart';
import 'package:news_bloc/modules/homepage/bloc/news/news_bloc.dart';
import 'package:news_bloc/modules/homepage/bloc/news_detail/news_detail_bloc.dart';
import 'package:news_bloc/modules/homepage/bloc/news_headline/news_headline_bloc.dart';
import 'package:news_bloc/modules/homepage/data/datasource/news_datasource.dart';
import 'package:news_bloc/modules/homepage/data/repository/news_repository.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(create: (context) => NewsRepository(newsDatasource: NewsDatasource())),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<NewsBloc>(
            create: (context) => NewsBloc(context.read<NewsRepository>()),
          ),
          BlocProvider<NewsHeadlineBloc>(
            create: (context) => NewsHeadlineBloc(context.read<NewsRepository>()),
          ),
          BlocProvider<NewsDetailBloc>(
            create: (context) => NewsDetailBloc(),
          )
        ],
        child: MaterialApp.router(
          routerConfig: globalRouter,
          title: 'News Bloc',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
        ),
      ),
    );
  }
}
